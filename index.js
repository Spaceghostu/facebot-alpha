import login from 'facebook-chat-api';
import fs from 'fs';
import dotenv from 'dotenv'
import wolfram from 'wolfram';

const wolframClient = wolfram.createClient(process.env.WOLFRAM_API_KEY || 'LK2Y7K-5U3AUU9JKW');
dotenv.config()

console.log("using credentials: %j", { email: process.env.USERNAME, password: process.env.PASSWORD.replace(/./g, '*') });

login({ email: process.env.USERNAME, password: process.env.PASSWORD }, (error, api) => {
    if (error) {
        return console.log("error: %j", error);
    }
    api.listen((error, message) => {
        if (error) {
            return console.log("error: %j", error);
        }
        console.log("message: %j", message);
        console.log('---------------------------------------------');
        wolframClient.query(message.body, (error, result) => {
            if (message.senderID == 100009285439140) {
                return;
            }
            fs.writeFile(`results/${message.body}.json`, JSON.stringify(result, null, 4));
            if (error) throw error
            console.log("result: %j", result);
            console.log('---------------------------------------------');
            const answer = result.map(value => {
                if (value.subpods[0].value) {
                    return value.title + ' - ' + value.subpods.map(subpod => subpod.value)
                }
                return null;
            }).filter(val => val !== null);
            console.log("answer: %j", answer);
            console.log('---------------------------------------------');
            console.log("an: %j", answer.splice(0, 1).join(',\n'));
            console.log('---------------------------------------------');
            if (answer.length) {
                api.sendMessage(answer.splice(0, 1).join(',\n') + '', message.threadID);
            }
        })
    });
});


